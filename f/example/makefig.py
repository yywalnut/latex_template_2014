#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot

def makefig():
    lm = 0.1
    rm = 0.1
    bm = 0.15
    tm = 0.15
    fig = pyplot.figure(figsize=(5.,4.))
    ax = fig.add_axes((lm, bm, (1. - lm - rm), (1. - tm - bm)))
    return fig, ax


if __name__ == "__main__":
    fig, ax = makefig()

    xs = [0., 2., 4., 5., 9.]
    ys = [1., 3., 8., 7., 11.]
    ax.plot(xs, ys, 'o')
    ax.set_xlabel('$x$, m')
    ax.set_ylabel('$\Phi_k$, eV')

    fig.savefig('out.pdf')
