\documentclass[12pt]{article}
 
% let's use some nice packages
\usepackage{times}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{array}
% set all margins to 2.5 cm
\usepackage[margin=2.5cm]{geometry}
% for fancy and easy modification of header and footer
\usepackage{fancyhdr}
% to change citations to [1-6] format
\usepackage{cite} 
% include hyperlinks in pdf file
  %\usepackage[plainpages=false]{hyperref}
%modify caption appearances
\usepackage[format=hang,font=small,labelfont=bf]{caption}
% to be able to use affiliations in the title
\usepackage{authblk}
\renewcommand\Affilfont{\itshape\small}
% also include colors
\usepackage{color}
\usepackage{pdfcolmk} % allow colors to span pages
% for double spacing
\usepackage{setspace}
% for highlighting text with \hl{}
\usepackage{soul}

%\setcounter{secnumdepth}{0} % suppress section numbers globally

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 % decide if run PdfTeX or LaTeX
\ifx\pdfoutput\undefined
% we are running LaTeX, not pdflatex
\usepackage{graphicx}
\else
% we are running pdflatex, so convert .eps files to .pdf
\usepackage[pdftex]{graphicx}
\usepackage{epstopdf}
\fi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
% define bottom line of paper, for DRAFT option
%\usepackage{prelim2e}\usepackage[none,bottom]{draftcopy}
%\draftcopyName{preprint / }{1.2}
%\renewcommand{\PrelimWords}{CO$_2$ on transition metals \jobname\ -- AAP}

\usepackage{pdflscape} % needed for landscape orientation
 
\usepackage[version=3]{mhchem} %for typesetting chemical formulae
\newcommand{\cen}[1]{\begin{equation}\ce{#1}\end{equation}}

% for ragged right in tables (needs the array package)
\newcolumntype{x}[1]{>{\raggedright\hspace{0pt}}p{#1}}
\newcommand{\tnl}{\tabularnewline}

% define a path for images
\graphicspath{{.},{./f/}}
 
% define common commands
\newcommand{\etal}{\textit{et al.}}
\newcommand{\dc}{$^{\circ}$C}

%% Comment commands %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Command to make text stand out that I need to follow up on
\newcommand{\fu}[1]{\textbf{\hl{#1\normalcolor}}}
%\renewcommand{\fu}[1]{}

% Uncomment first/second below to show/suppress comments.
\newcommand{\com}[1]{\footnotesize \color{red} \sffamily $\langle$~#1~$\rangle$ \rmfamily \normalcolor \normalsize}
%\renewcommand{\com}[1]{}

% Command to make text stand out for highlighting changes
\newcommand{\newtext}[1]{\color{blue}#1\normalcolor}
%\renewcommand{\newtext}[1]{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Standard figure and table definitions %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\standardfigure}[3]{
\begin{figure}
\centering
\includegraphics#2
\caption{#3\label{#1}}
\end{figure}
}

\newcommand{\standardtable}[4]{
\begin{table}
\centering
\caption{#2\label{#1}}
\begin{tabular}{#3}
#4
\end{tabular}
\end{table}
}
 

%% Title and authors %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Title}

\author{Author$^1$, Author$^2$, Author$^{1,\text{*}}$}
\affil{$^1$School of Engineering and $^2$Department of Chemistry, Brown University, Providence, Rhode Island, 02912, United States.\\ $\text{*}$contactaddress@brown.edu}
%\date{\today}
\date{}


\begin{document}
% show the title, author and date
\maketitle

\newcommand{\eco}{$E_\mathrm{B}[\ce{CO}]$}
\newcommand{\echo}{$E_\mathrm{B}[\ce{CHO}]$}
\newcommand{\eoh}{$E_\mathrm{B}[\ce{OH}]$}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Abstract %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{abstract}
\noindent
Include an abstract that summarizes motivation, results, conclusions. Generally include the graphical abstract (or ``table of contents'' graphic) right below the abstract, in the format requested by the journal.
\end{abstract}

\begin{center}\includegraphics[width=3.0in]{f/toc/drawing.pdf}\end{center}

{\singlespace
\footnotesize
\noindent \textbf{Keywords:} include, if, required
}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\doublespace

\newcommand{\mlh}{ML$_\text{H}$}
\newcommand{\ebh}{$E_\mathrm{B}[\ce{H}]$}

\newpage

The text of the manuscript goes here. You can include commands, as are seen a few lines above this, for commonly used symbols such as \mlh\ and \ebh. You can also include equations like the numbered one below

\begin{equation} \label{random-equation}
\frac{\partial^2 y}{\partial x^2} = 3
\end{equation}

\noindent or the one without the number below, which is suppressed by including an asterisk (*):

\begin{equation*}
\int_0^\infty \left( \frac{\partial h}{\partial T} \right)_p dT = \infty
\end{equation*}

\noindent or aligned equations as below

\begin{align}
f(x, y) &= (x + y) (x - y) \label{not-foiled} \\
		  &= x^2 - y^2 \label{foiled}
\end{align}

\noindent You will often want to suppress indentation after an equation with the \verb'\noindent' command. You can reference a labeled equation by referencing the word you tagged it with inside the label environment; for example, see equation~(\ref{not-foiled}) and (\ref{foiled}).

A new paragraph will start after a blank line. Chemical formulae are also easily included; for example, \ce{C6H12O6} is sugar. This is the combustion of sugar:

\cen{C6H12O6 + 6 O2 -> 6 CO2 + 6 H2O}

\noindent This is the reaction of sugar with a catalyst surface:

\cen{C6H12O6 + $*$ -> C6H12O6$*$}
 
 
Include literature references~\cite{Vanderbilt1990,Whitesides2004} with the cite package, which should point to references in the local bibliography file. The file is in bibtex format; numerous software packages can manage .bib files, including text editors as it is a plain text file. A nice cross-platform program is ``JabRef''. There is also a `.bst' file in the directory that dictates the output format of the reference list.

This is an example of a reference to an included figure within the manuscript, see Figure~\ref{example-figure}. Put all figures in the directory \verb'f'; try to make the figure name for the latex link (inside \verb'\ref{}') match the name of the file or directory within \verb'f', as in this example. The figure should generally be placed in the \LaTeX\ text file right after it's first reference; the float environment should place it in an ``ideal'' position.

\begin{figure}
\centering
\includegraphics[width=3.5in]{f/example/out.pdf}
\caption{ Describe the figure here.
\label{example-figure}}
\end{figure}



\section{Materials \& Methods}

Some manuscripts have sections; some communications do not. Include as appropriate.

\subsection{First experiments}

Subsections are also possible.

\paragraph{Subtle heading.} A paragraph heading is often an easier way to divide up a manuscript rather than using subsections.

\section*{Results \& Discussion}

Sometimes it looks better to not have numbers on the section headers, this is done with the asterisk or can be done globally at the top of the document.

\subsection*{First result}

Works also for subsections.  When a table is needed, you can use the format shown in Table~\ref{example-table}.  

\begin{table}
\centering
\caption{An example table\label{example-table}}
\begin{tabular}{lrr}
\hline \hline
Color & Number & Percent \\
\hline
Red & 2 & 40\% \\
Blue & 1 & 20\% \\
Green & 2 & 40\% \\
\hline \hline
\end{tabular}
\end{table}


\paragraph{Compiling.} Generally: run pdflatex $\rightarrow$ bibtex $\rightarrow$ pdflatex for a complete compile. Just run pdflatex if you don't care if references update. That is, for a full compile:

\begin{verbatim}
pdflatex v5-andy-updates
bibtex v5-andy-updates
pdflatex v5-andy-updates
\end{verbatim}

\noindent (You can also include the \verb'.tex' and \verb'.aux' extensions if you prefer.) If you need to put a temporary comment in --- for example, to make sure you remember to change something in your next draft or to make a note to another author --- use the custom \verb'\fu' command. \fu{Follow up on this suggestion, then delete this note.}  The internal notes can even be suppressed in the header lines of the \LaTeX\ source file if you need to.

\paragraph{File naming and versioning.} Be liberal about issuing version numbers. Generally, whenever you share a new version with your co-authors, make sure it has an updated version number as well as your initials and optionally a brief update of what changed. That is, a paper may go through the following versions:

\begin{verbatim}
v1-yz-first-version
v2-ap-edits
v3-yz-add-H-data
v4-yz-add conclusions
v5-ap-edits
v6-yz-polish
v7-ap-submit-to-JPCC
\end{verbatim}




\subsubsection*{ASSOCIATED CONTENT}

\paragraph{Supporting information.}
If supporting information is prepared --- as is increasingly becoming the norm in journal articles --- include a very short description of what can be found in that information here. Note the exact layout of these last sections may change with the journal formatting requirements.

\subsubsection*{AUTHOR INFORMATION}

\paragraph{Corresponding author.}
*Email: andrew\_peterson@brown.edu. Telephone +1 401-863-2153.

\paragraph{Acknowledgments.} Acknowledge any people who have provided key insight, but that is not significant enough to include on the author list. Provide grant numbers and acknowledgment to the CCV, etc.


% include the bibliography using bitex
\singlespace
\bibliographystyle{jpclp}
\bibliography{name-refs}

\end{document}
